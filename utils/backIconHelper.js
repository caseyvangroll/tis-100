const X = {
  0: 100,
  1: 400,
  2: 900,
}

const Y = {
  0: 250,
  1: 400,
}

const points = [
  // 0
  [X[0], 500],
  // 1
  [X[1], Y[0]],
  // 2
  [X[1], Y[1]],
  // 3
  [X[2], Y[1]],
  // 4
  [X[2], 1000 - Y[1]],
  // 5
  [X[1], 1000 - Y[1]],
  // 6
  [X[1], 1000 - Y[0]],
  // 7
  [X[0], 500],
]

const result = points.map(([x, y], i) => {
  return `${i === 0 ? 'M' : 'L'} ${x},${y}`
})
console.log(result.join(' '))
