import seedrandom from 'seedrandom'
import { RandomIntGenerator } from '../types'

function getRandomIntGenerator(seed: number): RandomIntGenerator {
  const rng = seedrandom(String(seed))

  return (inclusiveMin: number, inclusiveMax: number) => {
    return Math.floor(rng() * (inclusiveMax - inclusiveMin + 1) + inclusiveMin)
  }
}

export default getRandomIntGenerator
