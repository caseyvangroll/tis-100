import React from 'react'
import { DatabusArrow } from '..'
import { Egress } from '../../types'
import { Direction } from '../../constants'

export interface InputProps {
  id?: string
  column: number
  egress?: Egress
  style?: Record<string, unknown>
}

export const Input: React.FC<InputProps> = ({ id, column, egress, style = {} }) => {
  const fullStyle = {
    ...style,
    gridRow: 1,
    gridColumn: column * 2 + 1,
  }
  return (
    <>
      <div className="flex items-center pl-16 pointer-events-none" style={fullStyle}>
        {id}
      </div>
      <div className="flex w-auto h-8 pl-32 justify-start " style={fullStyle}>
        <DatabusArrow direction={Direction.Down} filled={egress?.direction === Direction.Down && egress?.data !== undefined} />
        <div className="self-center m-2">
          <p>{egress?.direction === Direction.Down && egress?.data}</p>
        </div>
      </div>
    </>
  )
}

export default React.memo(Input)
