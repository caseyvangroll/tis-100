import React from 'react'
import { StartFn, StartFastFn, StepFn, StopFn } from '../../types'
import { STEP_SPEED_MS, STEP_SPEED_FAST_MS } from '../../constants'
import { useWhatChanged } from '../../utils/useWhatChanged'

export interface ControlsProps {
  start: StartFn
  startFast: StartFastFn
  step: StepFn
  stop: StopFn
  isRunning: boolean
  stepSpeed: number | null
  testIndex: number
}

export const Controls: React.FC<ControlsProps> = ({ start, startFast, step, stop, isRunning, stepSpeed, testIndex }) => {
  const hasCompletedTests = testIndex > 2
  const isStopButtonDisabled = !isRunning
  const isStepButtonDisabled = hasCompletedTests
  const isRunButtonDisabled = hasCompletedTests || stepSpeed === STEP_SPEED_MS
  const isFastButtonDisabled = hasCompletedTests || stepSpeed === STEP_SPEED_FAST_MS

  return (
    <div id="controls" className="grid grid-cols-4 w-full gap-4">
      <button className="m-0 p-0 w-full" disabled={isStopButtonDisabled} onClick={stop}>
        STOP
      </button>
      <button className="m-0 p-0 w-full" disabled={isStepButtonDisabled} onClick={step}>
        STEP
      </button>
      <button className="m-0 p-0 w-full" disabled={isRunButtonDisabled} onClick={start}>
        RUN
      </button>
      <button className="m-0 p-0 w-full" disabled={isFastButtonDisabled} onClick={startFast}>
        FAST
      </button>
    </div>
  )
}

export default React.memo(Controls)
