import React from 'react'
import { Direction } from '../../constants'
import { SVG } from '..'

export interface DatabusArrowProps {
  direction: Direction
  filled: boolean
}

const DatabusArrow: React.FC<DatabusArrowProps> = ({ direction, filled }) => {
  if (filled) {
    switch (direction) {
      case Direction.Up:
        return <SVG.FilledArrowUp />
      case Direction.Down:
        return <SVG.FilledArrowDown />
      case Direction.Right:
        return <SVG.FilledArrowRight />
      default:
        return <SVG.FilledArrowLeft />
    }
  }
  switch (direction) {
    case Direction.Up:
      return <SVG.EmptyArrowUp />
    case Direction.Down:
      return <SVG.EmptyArrowDown />
    case Direction.Right:
      return <SVG.EmptyArrowRight />
    default:
      return <SVG.EmptyArrowLeft />
  }
}

export default DatabusArrow
