import React from 'react'
import classNames from 'classnames'
import LinesOfCode from './LinesOfCode'
import { DatabusArrow } from '..'
import { Egress, SetNodeLinesOfCodeFn } from '../../types'
import { Direction } from '../../constants'

export interface NodeProps {
  row: number
  column: number
  cursorIndex: number
  linesOfCode: Array<string>
  curLineIndex: number
  isDisabled: boolean
  hasError: boolean
  acc: number
  bak: number
  isRunning: boolean
  egress?: Egress
  hasUp?: boolean
  hasRight?: boolean
  hasDown?: boolean
  hasLeft?: boolean
  style?: Record<string, unknown>
  setNodeLinesOfCode: SetNodeLinesOfCodeFn
}

export const Node: React.FC<NodeProps> = ({
  row,
  column,
  cursorIndex,
  linesOfCode,
  curLineIndex,
  isDisabled,
  hasError,
  acc,
  bak,
  isRunning,
  egress,
  hasUp,
  hasRight,
  hasDown,
  hasLeft,
  style = {},
  setNodeLinesOfCode,
}) => {
  const gridRow = row * 2 + 2 // Top row of grid is always for input egress
  const gridColumn = column * 2 + 1

  const arrowUp = !isDisabled && hasUp && (
    <div className="flex w-auto h-8 pr-32 justify-end" style={{ gridRow: gridRow - 1, gridColumn }} data-testid="arrow-up">
      <div className="flex items-center p-2">
        <p>{egress?.direction === Direction.Up && egress?.data}</p>
      </div>
      <DatabusArrow direction={Direction.Up} filled={egress?.direction === Direction.Up && egress?.data !== undefined} />
    </div>
  )
  const arrowRight = !isDisabled && hasRight && (
    <div className="flex flex-col w-8 h-auto pb-32  justify-end" style={{ gridRow: gridRow, gridColumn: gridColumn + 1 }} data-testid="arrow-right">
      <div className="flex items-center p-2">
        <p>{egress?.direction === Direction.Right && egress?.data}</p>
      </div>
      <DatabusArrow direction={Direction.Right} filled={egress?.direction === Direction.Right && egress?.data !== undefined} />
    </div>
  )
  const arrowDown = !isDisabled && hasDown && (
    <div className="flex w-auto h-8 pl-32 justify-start" style={{ gridRow: gridRow + 1, gridColumn }} data-testid="arrow-down">
      <DatabusArrow direction={Direction.Down} filled={egress?.direction === Direction.Down && egress?.data !== undefined} />
      <div className="flex items-center p-2">
        <p>{egress?.direction === Direction.Down && egress?.data}</p>
      </div>
    </div>
  )
  const arrowLeft = !isDisabled && hasLeft && (
    <div className="flex flex-col w-8 h-auto pt-32 justify-start" style={{ gridRow, gridColumn: gridColumn - 1 }} data-testid="arrow-left">
      <DatabusArrow direction={Direction.Left} filled={egress?.direction === Direction.Left && egress?.data !== undefined} />
      <div className="flex items-center p-2">
        <p>{egress?.direction === Direction.Left && egress?.data}</p>
      </div>
    </div>
  )

  return (
    <>
      <div
        className={classNames('node grid grid-rows-4 grid-cols-4 p-px w-52 h-52', {
          isDisabled,
        })}
        style={{
          ...style,
          gridRow,
          gridColumn,
        }}
        data-testid="node"
      >
        <LinesOfCode
          row={row}
          column={column}
          cursorIndex={cursorIndex}
          linesOfCode={linesOfCode}
          curLineIndex={curLineIndex}
          isDisabled={isDisabled}
          hasError={hasError}
          isRunning={isRunning}
          setNodeLinesOfCode={setNodeLinesOfCode}
        />
        <div
          className={classNames('acc data-container border-t-2 border-r-2 flex flex-col self-center items-center justify-center text-center p-px w-full h-full', { isDisabled })}
          data-testid="acc"
        >
          <p className="leading-5">{!isDisabled && 'ACC'}</p>
          <p className="leading-5 text-neutral-400">{!isDisabled && String(acc)}</p>
        </div>

        <div
          className={classNames('bak data-container border-t-2 border-r-2 flex flex-col self-center items-center justify-center text-center p-px w-full h-full', { isDisabled })}
          data-testid="bak"
        >
          <p className="leading-5">{!isDisabled && 'BAK'}</p>
          <p className="leading-5 text-neutral-400">{!isDisabled && String(bak)}</p>
        </div>

        <div
          className={classNames('last data-container border-t-2 border-r-2 flex flex-col self-center items-center justify-center text-center p-px w-full h-full', { isDisabled })}
          data-testid="last"
        >
          <p className="leading-5">{!isDisabled && 'LAST'}</p>
          <p className="leading-5 text-neutral-400">{!isDisabled && '-'}</p>
        </div>
        <div
          className={classNames('mode data-container border-t-2 border-r-2 border-b-2 flex flex-col self-center items-center justify-center text-center p-px w-full h-full', {
            isDisabled,
          })}
          data-testid="mode"
        >
          <p className="leading-5">{!isDisabled && 'MODE'}</p>
          <p className="leading-5 text-neutral-400">{!isDisabled && '-'}</p>
        </div>
      </div>
      {arrowUp}
      {arrowRight}
      {arrowDown}
      {arrowLeft}
    </>
  )
}

export default React.memo(Node)
