import classNames from 'classnames'
import React, { useRef, useEffect } from 'react'
import { SetNodeLinesOfCodeFn } from '../../types'

// document.execCommand("defaultParagraphSeparator", false, "span");
export interface LinesOfCodeProps {
  row: number
  column: number
  cursorIndex: number
  linesOfCode: Array<string>
  curLineIndex: number
  isRunning: boolean
  isDisabled: boolean
  hasError: boolean
  setNodeLinesOfCode: SetNodeLinesOfCodeFn
}

export const LinesOfCode: React.FC<LinesOfCodeProps> = ({ row, column, cursorIndex, linesOfCode, curLineIndex, isRunning, isDisabled, hasError, setNodeLinesOfCode }) => {
  const inputRef = useRef<HTMLTextAreaElement>(null)
  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const nextCursorIndex = event.target.selectionStart || 0
    const nextTextAreaValue = event.target.value
    setNodeLinesOfCode(row, column, nextTextAreaValue.split('\n'), nextCursorIndex)
  }

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.selectionStart = cursorIndex
      inputRef.current.selectionEnd = cursorIndex
    }
  }, [inputRef, cursorIndex])

  if (isDisabled) {
    return (
      <div className="code-container disabled-mode border-2" data-test-id="lines-of-code">
        <pre className="code-output">
          <code>
            <span>COMMUNICATION</span>
            <span>FAILURE</span>
          </code>
        </pre>
      </div>
    )
  }

  if (isRunning) {
    return (
      <div className="code-container run-mode border-2">
        <pre className="code-output">
          <code>
            {linesOfCode.map((lineOfCode, i) => (
              <span
                key={i}
                className={classNames({
                  isCurrentLine: curLineIndex === i,
                })}
              >
                {lineOfCode}
              </span>
            ))}
          </code>
        </pre>
      </div>
    )
  }

  return (
    <div
      className={classNames('code-container', 'edit-mode', 'border-2', {
        hasError,
        isDisabled,
      })}
      data-test-id="lines-of-code"
    >
      <textarea className="code-input whitespace-nowrap overflow-hidden" value={linesOfCode.join('\n')} onChange={handleChange} ref={inputRef} />
    </div>
  )
}

export default LinesOfCode
