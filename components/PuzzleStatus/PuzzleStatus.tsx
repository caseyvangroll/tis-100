import React from 'react'

export interface PuzzleStatusProps {
  description: Array<string>
  testIndex: number
  isRunning: boolean
}

export const PuzzleStatus: React.FC<PuzzleStatusProps> = ({ description, testIndex, isRunning }) => {
  let lines = description
  if (isRunning) {
    lines = []
    for (let i = 0; i < testIndex; i += 1) {
      lines.push(`TEST ${i + 1} PASSED`)
    }
    if (testIndex <= 2) {
      lines.push(`RUNNING TEST ${testIndex + 1}...`)
    } else {
      lines.push('SUCCESS!')
    }
  }
  return (
    <div className="flex flex-col mb-4 h-28 p-3 border-2 overflow-y-auto text-sm w-full" style={{ minHeight: '6rem' }}>
      {lines.map((line, i) => (
        <span key={i} className="pointer-events-none">
          &gt; {line}
        </span>
      ))}
    </div>
  )
}

export default React.memo(PuzzleStatus)
