import React from 'react'
import Link from 'next/link'
import { HELP_URL } from '../../constants'

export const Nav: React.FC = () => {
  return (
    <nav className="grid grid-cols-4 w-full gap-4 mb-6">
      <Link href="/puzzles" passHref>
        <button className="box-shadow m-0 p-0 w-full">
          <a>BACK</a>
        </button>
      </Link>
      <div />
      <div />
      <button className="box-shadow m-0 p-0 w-full">
        <a href={HELP_URL} target="_blank" rel="noreferrer">
          HELP
        </a>
      </button>
    </nav>
  )
}

export default React.memo(Nav)
