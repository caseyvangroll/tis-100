import React from 'react'
import { TestOutputType } from '../../types'

export interface TestOutputsDisplayProps {
  outputs: Array<TestOutputType>
}

export const TestOutputsDisplay: React.FC<TestOutputsDisplayProps> = ({ outputs }) => {
  return (
    <div id="test-outputs" className="flex">
      {outputs.map(({ id, expectedValues, valuesReceived }) => {
        return (
          <div key={id} className="test-output-container">
            <div className="test-output-id">{id}</div>
            <div className="test-output-values border-2">
              <div className="test-output-expected border-r-2 leading-[0.85rem]">
                {expectedValues.map((value, i) => (
                  <span key={i}>{value}</span>
                ))}
              </div>
              <div className="test-output-actual leading-[0.85rem]">
                {valuesReceived.map((value, i) => {
                  const className = value !== expectedValues[i] ? 'bg-red-500' : ''
                  return (
                    <span key={i} className={className}>
                      {value}
                    </span>
                  )
                })}
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default TestOutputsDisplay
