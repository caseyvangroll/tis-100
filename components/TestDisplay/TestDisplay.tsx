import React from 'react'
import { TestInputType, TestOutputType } from '../../types'
import TestInputsDisplay from './TestInputsDisplay'
import TestOutputsDisplay from './TestOutputsDisplay'

export interface TestDisplayProps {
  inputs: Array<TestInputType>
  outputs: Array<TestOutputType>
  isRunning: boolean
}

export const TestDisplay: React.FC<TestDisplayProps> = ({ inputs, outputs, isRunning }) => {
  return (
    <div className="flex justify-around w-full mb-4">
      <TestInputsDisplay inputs={inputs} isRunning={isRunning} />
      <TestOutputsDisplay outputs={outputs} />
    </div>
  )
}

export default React.memo(TestDisplay)
