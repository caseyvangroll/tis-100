import React from 'react'
import { TestInputType } from '../../types'

export interface TestInputsDisplayProps {
  inputs: Array<TestInputType>
  isRunning: boolean
}

export const TestInputsDisplay: React.FC<TestInputsDisplayProps> = ({ inputs, isRunning }) => {
  return (
    <div className="flex mr-4">
      {inputs.map(({ id, valuesToSend, curValueToSendIndex }) => {
        return (
          <div key={id} className="test-input-container">
            <div className="test-input-id">{id}</div>
            <div className="flex flex-col border-2 p-1 text-center leading-[0.85rem]">
              {valuesToSend.map((value, i) => {
                let style = {}
                if (curValueToSendIndex === i && isRunning) {
                  style = {
                    backgroundColor: 'white',
                    color: 'black',
                  }
                }
                return (
                  <span key={i} style={style}>
                    {value}
                  </span>
                )
              })}
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default TestInputsDisplay
