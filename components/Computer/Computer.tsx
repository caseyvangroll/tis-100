import React from 'react'
import { Node, Input, Output } from '..'
import { NodeType } from '../../constants'
import { ComputeNode, SetNodeLinesOfCodeFn } from '../../types'

export interface ComputerProps {
  nodes: Array<ComputeNode>
  isRunning: boolean
  setNodeLinesOfCode: SetNodeLinesOfCodeFn
}

const Computer: React.FC<ComputerProps> = ({ nodes, isRunning, setNodeLinesOfCode }) => {
  return (
    <div id="computer" className="p-1 grid gap-2">
      {nodes.map((node) => {
        if (node.type === NodeType.Input) {
          return <Input key={node.id} {...node} />
        }

        if (node.type === NodeType.Output) {
          return <Output key={node.id} {...node} />
        }

        return <Node key={node.id} setNodeLinesOfCode={setNodeLinesOfCode} isRunning={isRunning} {...node} />
      })}
    </div>
  )
}

export default Computer
