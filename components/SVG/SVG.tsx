import EmptyArrowLeft from './EmptyArrowLeft'
import EmptyArrowRight from './EmptyArrowRight'
import EmptyArrowUp from './EmptyArrowUp'
import EmptyArrowDown from './EmptyArrowDown'
import FilledArrowLeft from './FilledArrowLeft'
import FilledArrowRight from './FilledArrowRight'
import FilledArrowUp from './FilledArrowUp'
import FilledArrowDown from './FilledArrowDown'

const SVG = {
  EmptyArrowLeft,
  EmptyArrowRight,
  EmptyArrowUp,
  EmptyArrowDown,
  FilledArrowLeft,
  FilledArrowRight,
  FilledArrowUp,
  FilledArrowDown,
}

export default SVG
