const EmptyArrowUp = () => (
  <svg viewBox="0,0 216,510" width="216" height="510" className="h-full w-auto">
    <polyline points="10,160.0 70,160.0 70,506 150,506 150,160.0 210,160.0 110,6 10,160.0 70,160.0" stroke="#FFFFFF" strokeWidth="10" fill="none" />
  </svg>
)

export default EmptyArrowUp
