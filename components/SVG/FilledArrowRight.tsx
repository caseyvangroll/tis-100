const FilledArrowRight = () => (
  <svg viewBox="0,0 510,216" width="510" height="216" className="w-full h-auto">
    <polyline
      points="10,160.0 70,160.0 70,506 150,506 150,160.0 210,160.0 110,6 10,160.0 70,160.0"
      stroke="#FFFFFF"
      strokeWidth="10"
      fill="#FFFFFF"
      transform="rotate(90) translate(0, -510)"
    />
  </svg>
)

export default FilledArrowRight
