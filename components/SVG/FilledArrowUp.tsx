const FilledArrowUp = () => (
  <svg viewBox="0,0 216,510" width="216" height="510" className="h-full w-auto">
    <polyline points="10,160 70,160 70,506 150,506 150,160 210,160 110,6 10,160 70,160" stroke="#FFFFFF" strokeWidth="10" fill="#FFFFFF" />
  </svg>
)

export default FilledArrowUp
