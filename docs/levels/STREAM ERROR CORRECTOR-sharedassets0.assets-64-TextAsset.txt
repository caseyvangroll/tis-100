0 TextAsset Base
 1 string m_Name = "STREAM ERROR CORRECTOR"
 1 string m_Script = "-- The function get_name() should return a single string that is the name of the puzzle.
--
function get_name()
	return "SIGNAL ERROR CORRECTOR"
end

function get_creator()
	return "KEITH H"
end

-- The function get_description() should return an array of strings, where each string is
-- a line of description for the puzzle. The text you return from get_description() will 
-- be automatically formatted and wrapped to fit inside the puzzle information box.
--
function get_description()
	return {
		"COPY VALUES FROM IN.A TO OUT.A",
		"COPY VALUES FROM IN.B TO OUT.B",
		"IF A VALUE IS NEGATIVE, INSTEAD COPY THE VALUE FROM OTHER INPUT",
		"IN.A AND IN.B WILL NEVER BE NEGATIVE ON SAME INPUT CYCLE",
	}
end

-- The function get_streams() should return an array of streams. Each stream is described
-- by an array with exactly four values: a STREAM_* value, a name, a position, and an array
-- of integer values between -999 and 999 inclusive.
--
-- STREAM_INPUT: An input stream containing up to 39 numerical values.
-- STREAM_OUTPUT: An output stream containing up to 39 numerical values.
-- STREAM_IMAGE: An image output stream, containing exactly 30*18 numerical values between 0
--               and 4, representing the full set of "pixels" for the target image.
--
-- NOTE: Arrays in Lua are implemented as tables (dictionaries) with integer keys that start
--       at 1 by convention. The sample code below creates an input array of 39 random values
--       and an output array that doubles all of the input values.
--
-- NOTE: To generate random values you should use math.random(). However, you SHOULD NOT seed
--       the random number generator with a new seed value, as that is how TIS-100 ensures that
--       the first test run is consistent for all users, and thus something that allows for the
--       comparison of cycle scores.
--
-- NOTE: Position values for streams should be between 0 and 3, which correspond to the far
--       left and far right of the TIS-100 segment grid. Input streams will be automatically
--       placed on the top, while output and image streams will be placed on the bottom.
--
function get_streams()
	local in_a = {}
	local in_b = {}
	local out_a = {}
	local out_b = {}
	for i = 1, 39 do
		local r = math.random(1, 4)
		local a = math.random(10, 99)
		local b = math.random(10, 99)
		if r == 1 then
			in_a[i] = -1
			in_b[i] = b
			out_a[i] = b
			out_b[i] = b
		elseif r == 2 then
			in_a[i] = a
			in_b[i] = -1
			out_a[i] = a
			out_b[i] = a
		else
			in_a[i] = a
			in_b[i] = b
			out_a[i] = a
			out_b[i] = b
		end
	end
	return {
		{ STREAM_INPUT, "IN.A", 1, in_a },
		{ STREAM_INPUT, "IN.B", 2, in_b },
		{ STREAM_OUTPUT, "OUT.A", 1, out_a },
		{ STREAM_OUTPUT, "OUT.B", 2, out_b },
	}
end

-- The function get_layout() should return an array of exactly 12 TILE_* values, which
-- describe the layout and type of tiles found in the puzzle.
--
-- TILE_COMPUTE: A basic execution node (node type T21).
-- TILE_MEMORY: A stack memory node (node type T30).
-- TILE_DAMAGED: A damaged execution node, which acts as an obstacle.
--
function get_layout()
	return { 
		TILE_DAMAGED, 	TILE_COMPUTE, 	TILE_COMPUTE, 	TILE_JOURNAL,
		TILE_COMPUTE, 	TILE_COMPUTE,	TILE_COMPUTE, 	TILE_COMPUTE,
		TILE_COMPUTE, 	TILE_COMPUTE,	TILE_COMPUTE, 	TILE_COMPUTE,
	}
end
"
 1 string m_PathName = ""
