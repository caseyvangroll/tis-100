0 TextAsset Base
 1 string m_Name = "CHARACTER EMITTER"
 1 string m_Script = "function get_name()
  return "CHARACTER TERMINAL"
end

function get_creator()
	return "JESSE S"
end

function get_description()
  return {
    "READ A CHARACTER CODE FROM IN",
    "IF 0, START A NEW LINE",
    "IF NOT, PRINT THE CHARACTER",
  }
end

function get_streams()
  image = {}
  for i = 1,30*18 do
    image[i] = 0
  end

  input = {}
  for i = 0,38 do
    x = math.random(1,4)
    input[i] = x
  end
  input[39] = 0

  -- these values are choosen to allow a bit of undefined behavior; I do not pick whether a 0 at after the 10th
  -- character should be an empty line.  The prerolled sample will not have that, so I avoid testing the user on that
  -- case because it seemed unfair.  I think both ways could be valid.
  input[math.random(12,16)] = 0
  input[math.random(28,31)] = 0

  render_image(image, input)

  return {
    { STREAM_INPUT, "IN", 1, input },
    { STREAM_IMAGE, "", 2, image },
  }
end

function render_image(image, input) 
  x = -1
  y = 0
  for i=0,37 do
    if input[i] == 0 or x == 9 then
      x = 0
      y=y+1
    else
      x=x+1
    end
    render_character(image, x * 3, y * 3, input[i+1])
  end
end

-- place the character c into the image at x y
function render_character(image, x, y, c) 
  char = get_char(c)
  for a=1,2 do
    for b=1,2 do
      if char[a][b] == 1 then
        -- color for the character is set here
        image[x+a + (y+b-1)*30] = 3 
      end
    end
  end
end

-- replace 2d arrays for alternative characters
function get_char(code) 
  if (code == 0) then
    return {{0,0}, {0,0}}
  elseif (code == 1) then
    return {{1,1}, {0,0}}
  elseif (code == 2) then
    return {{1,0}, {0,1}}
  elseif (code == 3) then
    return {{0,1}, {1,0}}
  else
    return {{1,1}, {1,0}}
  end
end

function get_layout()
  return {
    TILE_MEMORY,   TILE_COMPUTE,   TILE_COMPUTE,   TILE_JOURNAL,
    TILE_COMPUTE,   TILE_COMPUTE,   TILE_COMPUTE,   TILE_COMPUTE,
    TILE_MEMORY,   TILE_COMPUTE,   TILE_COMPUTE,   TILE_COMPUTE,
  }
end"
 1 string m_PathName = ""
