0 TextAsset Base
 1 string m_Name = "SIGNAL NORMALIZER"
 1 string m_Script = "-- The function get_name() should return a single string that is the name of the puzzle.
--
function get_name()
	return "SEQUENCE NORMALIZER"
end

function get_creator()
	return "DECLAN R"
end

-- The function get_description() should return an array of strings, where each string is
-- a line of description for the puzzle. The text you return from get_description() will 
-- be automatically formatted and wrapped to fit inside the puzzle information box.
--
function get_description()
	return {
        "SEQUENCES ARE TERMINATED WITH -1",
        "READ A SEQUENCE FROM IN",
        "FIND MINIMUM VALUE IN SEQUENCE",
        "SUBTRACT THE MINIMUM VALUE FROM EACH VALUE IN SEQUENCE",
        "WRITE THE SEQUENCE TO OUT",
    }
end

-- The function get_streams() should return an array of streams. Each stream is described
-- by an array with exactly four values: a STREAM_* value, a name, a position, and an array
-- of integer values between -999 and 999 inclusive.
--
-- STREAM_INPUT: An input stream containing up to 39 numerical values.
-- STREAM_OUTPUT: An output stream containing up to 39 numerical values.
-- STREAM_IMAGE: An image output stream, containing exactly 30*18 numerical values between 0
--               and 4, representing the full set of "pixels" for the target image.
--
-- NOTE: Arrays in Lua are implemented as tables (dictionaries) with integer keys that start
--       at 1 by convention. The sample code below creates an input array of 39 random values
--       and an output array that doubles all of the input values.
--
-- NOTE: To generate random values you should use math.random(). However, you SHOULD NOT seed
--       the random number generator with a new seed value, as that is how TIS-100 ensures that
--       the first test run is consistent for all users, and thus something that allows for the
--       comparison of cycle scores.
--
-- NOTE: Position values for streams should be between 0 and 3, which correspond to the far
--       left and far right of the TIS-100 segment grid. Input streams will be automatically
--       placed on the top, while output and image streams will be placed on the bottom.
--
function get_streams()
	input = {}
	output = {}
    -- Current sequence from 'input'
    cur_seq = {}
    -- Index of last sequence terminating digit in 'input'
    last_term = 0
    
    i = 1
    while i ~= 39 do
		input[i] = math.random(1, 99)
        cur_seq[i - last_term] = input[i]
        
        -- Possibly end sequence ensuring that the it is at least of length 3
        -- and no longer than 8.
        -- Also ensure final sequence is ended properly - may cause shorter sequence length.
        if (math.random(1,3) % 3 == 0 and #cur_seq > 2) or (#cur_seq > 7) or (i == 37) then    
            -- Generate 'output'
            min_in_seq = math.min(unpack(cur_seq))
            for j = last_term+1,i do
                output[j] = input[j] - min_in_seq
            end
            
            -- Add the sequence terminating -1
            i = i + 1
            input[i] = -1
            output[i] = -1
            
            -- Update position of last zero in 'input'
            last_term = i
            
            -- Reset cur_seq
            for j = #cur_seq,1,-1 do
                table.remove(cur_seq, j)
            end
        end
        i = i + 1
	end
    
	return {
		{ STREAM_INPUT, "IN", 1, input },
		{ STREAM_OUTPUT, "OUT", 2, output },
	}
end

-- The function get_layout() should return an array of exactly 12 TILE_* values, which
-- describe the layout and type of tiles found in the puzzle.
--
-- TILE_COMPUTE: A basic execution node (node type T21).
-- TILE_MEMORY: A stack memory node (node type T30).
-- TILE_DAMAGED: A damaged execution node, which acts as an obstacle.
--
function get_layout()
	return { 
		TILE_COMPUTE, 	TILE_COMPUTE, 	TILE_MEMORY, 	TILE_COMPUTE,
		TILE_COMPUTE, 	TILE_COMPUTE,	TILE_COMPUTE, 	TILE_MEMORY,
		TILE_COMPUTE, 	TILE_JOURNAL,	TILE_COMPUTE, 	TILE_COMPUTE,
	}
end"
 1 string m_PathName = ""
