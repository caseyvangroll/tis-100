import { scanToNextCommand } from './helpers'
import { nodeDefaults } from '../nodes'
import { NodeType, Mode } from '../../../constants'

describe('scanToNextCommand', () => {
  describe('forward', () => {
    test('should result in no change if no lines of code', () => {
      const node = {
        ...nodeDefaults,
        id: 'jezNode',
        row: 0,
        column: 0,
        linesOfCode: [],
        curLineIndex: -1,
      }
      const expected = {
        ...node,
      }

      expect(scanToNextCommand(node)).toEqual(expected)
    })

    test('should be no-op if no command in lines of code', () => {
      const node = {
        id: 'node[0,1]',
        type: NodeType.Compute,
        row: 0,
        column: 1,
        cursorIndex: 0,
        linesOfCode: ['', 'LABEL:', '#COMMENT'],
        curLineIndex: -1,
        isDisabled: false,
        hasError: false,
        acc: 0,
        bak: 0,
        mode: Mode.Idle,
        hasUp: false,
        hasRight: true,
        hasDown: true,
        hasLeft: true,
      }
      expect(scanToNextCommand(node)).toEqual({ ...node })
    })
  })
  // describe('reverse', () => {
  //   test('should result in no change if no lines of code', () => {
  //     const node = {

  //     }
  //     const result = scanToNextCommand(node, );
  //   })
  // })
})
