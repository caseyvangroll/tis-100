import { ComputeNode, AdjacentNodesType } from '../../../types'
import { Direction } from '../../../constants'
import { commands } from '.'

export const isInteger = (value: string): boolean => /\d+/.test(value)

export const resolveOperand = (node: ComputeNode, operand: string, adjacentNodes: AdjacentNodesType): number | undefined => {
  if (isInteger(operand)) {
    return Number(operand)
  } else if (operand === 'ACC') {
    return Number(node.acc)
  } else if (operand === Direction.Up && adjacentNodes.upNode?.egress?.direction === Direction.Down) {
    return Number(adjacentNodes.upNode.egress.data)
  } else if (operand === Direction.Right && adjacentNodes.rightNode?.egress?.direction === Direction.Left) {
    return Number(adjacentNodes.rightNode.egress.data)
  } else if (operand === Direction.Down && adjacentNodes.downNode?.egress?.direction === Direction.Up) {
    return Number(adjacentNodes.downNode.egress.data)
  } else if (operand === Direction.Left && adjacentNodes.leftNode?.egress?.direction === Direction.Right) {
    return Number(adjacentNodes.leftNode.egress.data)
  }
}

export const isCommand = (lineOfCode: string = ''): boolean => commands.some(({ matcher }) => matcher.test(lineOfCode))

export const isLabel = (lineOfCode: string): boolean => /^[\s,]*[^#\s,]+:(#.*)?$/.test(lineOfCode)

export const isEmpty = (lineOfCode: string): boolean => /^[\s,]*(#.*)?$/.test(lineOfCode)

export const isDirection = (operand: string): boolean => operand === Direction.Up || operand === Direction.Right || operand === Direction.Down || operand === Direction.Left

export function doLinesOfCodeHaveError(linesOfCode: Array<string>): boolean {
  return !linesOfCode.every((lineOfCode) => isCommand(lineOfCode) || isLabel(lineOfCode) || isEmpty(lineOfCode))
}

export function scanToNextCommand(node: ComputeNode, reverse?: boolean): ComputeNode {
  if (!node.linesOfCode.length || !node.linesOfCode.some(isCommand)) {
    return node
  }

  const delta = reverse ? -1 : 1

  let nextCurLineIndex = node.curLineIndex
  let scannedAllLines = false
  const scan = () => {
    nextCurLineIndex += delta
    if (nextCurLineIndex < 0) {
      nextCurLineIndex = node.linesOfCode.length - 1
    }
    if (nextCurLineIndex >= node.linesOfCode.length) {
      nextCurLineIndex = -1
    }
    scannedAllLines = scannedAllLines || nextCurLineIndex === node.curLineIndex
  }

  scan()

  while (!scannedAllLines && !isCommand(node.linesOfCode[nextCurLineIndex])) {
    scan()
  }

  return {
    ...node,
    curLineIndex: nextCurLineIndex,
  }
}
