import { executeJnz } from './JNZ'
import { nodeDefaults } from '../../nodes'

describe('executeJnz', () => {
  const onIngress = jest.fn()
  const onEgress = jest.fn()
  afterEach(() => {
    onIngress.mockClear()
    onEgress.mockClear()
  })

  test('jumps to first command after label if acc is greater than zero', () => {
    const node = {
      ...nodeDefaults,
      id: 'jnzNode',
      row: 0,
      column: 0,
      linesOfCode: ['JNZ TEST', 'ADD 1', 'TEST:', 'ADD 1'],
      curLineIndex: 0,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 3,
    }

    const result = executeJnz({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('jumps to first command after label if acc is less than zero (with wrap)', () => {
    const node = {
      ...nodeDefaults,
      id: 'jnzNode',
      row: 0,
      column: 0,
      linesOfCode: ['TEST:', 'ADD 1', 'JNZ TEST', 'ADD 1'],
      curLineIndex: 2,
      acc: -1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 1,
    }

    const result = executeJnz({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('does not jump to label if acc is zero', () => {
    const node = {
      ...nodeDefaults,
      id: 'jnzNode',
      row: 0,
      column: 0,
      linesOfCode: ['JNZ TEST', 'ADD 1', 'TEST:', 'ADD 1'],
      curLineIndex: 0,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 1,
    }

    const result = executeJnz({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })
})
