import { ComputeNode, CommandExecuteFnArgs } from '../../../../types'
import { scanToNextCommand } from '../helpers'

export function executeSwp({ node }: CommandExecuteFnArgs): ComputeNode {
  return scanToNextCommand({
    ...node,
    acc: node.bak,
    bak: node.acc,
  })
}
