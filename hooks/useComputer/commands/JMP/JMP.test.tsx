import { executeJmp } from './JMP'
import { nodeDefaults } from '../../nodes'

describe('executeJmp', () => {
  const onIngress = jest.fn()
  const onEgress = jest.fn()
  afterEach(() => {
    onIngress.mockClear()
    onEgress.mockClear()
  })

  test('jumps to first command after label', () => {
    const node = {
      ...nodeDefaults,
      id: 'jmpNode',
      row: 0,
      column: 0,
      linesOfCode: ['JMP TEST', 'ADD 1', 'TEST:', 'ADD 1'],
      curLineIndex: 0,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 3,
    }

    const result = executeJmp({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('jumps to first command after label (with wrap)', () => {
    const node = {
      ...nodeDefaults,
      id: 'jmpNode',
      row: 0,
      column: 0,
      linesOfCode: ['TEST:', 'ADD 1', 'JMP TEST', 'ADD 1'],
      curLineIndex: 2,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 1,
    }

    const result = executeJmp({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })
})
