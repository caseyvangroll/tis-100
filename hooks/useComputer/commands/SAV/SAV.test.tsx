import { executeSav } from './SAV'
import { nodeDefaults } from '../../nodes'

describe('executeSav', () => {
  const onIngress = jest.fn()
  const onEgress = jest.fn()
  afterEach(() => {
    onIngress.mockClear()
    onEgress.mockClear()
  })

  test('copies acc to bak', () => {
    const node = {
      ...nodeDefaults,
      id: 'savNode',
      row: 0,
      column: 0,
      linesOfCode: ['SAV'],
      curLineIndex: 0,
      acc: 2,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      bak: 2,
    }

    const result = executeSav({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })
})
