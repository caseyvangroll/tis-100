import { ComputeNode, CommandExecuteFnArgs } from '../../../../types'
import { scanToNextCommand } from '../helpers'

export function executeJro({ node, lineOfCode }: CommandExecuteFnArgs): ComputeNode {
  const [, offset] = lineOfCode.split(/\s/g)

  let nextNode = { ...node }
  const reverse = Number(offset) < 0
  for (let i = 0; i < Math.abs(Number(offset)); i += 1) {
    nextNode = scanToNextCommand(nextNode, reverse)
    if ((nextNode.curLineIndex === 0 && reverse) || (nextNode.curLineIndex === nextNode.linesOfCode.length - 1 && !reverse)) {
      break
    }
  }

  return nextNode
}
