import { executeJro } from './JRO'
import { nodeDefaults } from '../../nodes'

describe('executeJro', () => {
  const onIngress = jest.fn()
  const onEgress = jest.fn()
  afterEach(() => {
    onIngress.mockClear()
    onEgress.mockClear()
  })

  test('jumps forward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['JRO 2', 'ADD 1', 'ADD 1'],
      curLineIndex: 0,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 2,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('stops at last line if tries to jump too far forward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['JRO 20', 'ADD 1', 'ADD 1', 'ADD 1'],
      curLineIndex: 0,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 3,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('ignores comments and labels when jumping forward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['JRO 2', '#COMMENT', 'ADD 1', 'LABEL:', 'ADD 1'],
      curLineIndex: 0,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 4,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('jumps backward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['ADD 1', 'ADD 1', 'ADD 1', 'JRO -2'],
      curLineIndex: 3,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 1,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('stops at line 0 if tries to jump too far backward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['ADD 1', 'ADD 1', 'ADD 1', 'JRO -10'],
      curLineIndex: 3,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 0,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })

  test('ignores comments and labels when jumping backward', () => {
    const node = {
      ...nodeDefaults,
      id: 'jroNode',
      row: 0,
      column: 0,
      linesOfCode: ['ADD 1', '#COMMENT', 'ADD 1', 'LABEL:', 'JRO -2'],
      curLineIndex: 4,
      acc: 1,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = {
      ...node,
      curLineIndex: 0,
    }

    const result = executeJro({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })
})
