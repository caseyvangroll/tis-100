import { ComputeNode, CommandExecuteFnArgs } from '../../../../types'
import { scanToNextCommand } from '../helpers'

export function prepareNop(node: ComputeNode, lineOfCode: string): ComputeNode {
  return node
}

export function executeNop({ node }: CommandExecuteFnArgs): ComputeNode {
  return scanToNextCommand(node)
}
