import { executeNop } from './NOP'
import { scanToNextCommand } from '../helpers'
import { nodeDefaults } from '../../nodes'

describe('executeNop', () => {
  const onIngress = jest.fn()
  const onEgress = jest.fn()
  afterEach(() => {
    onIngress.mockClear()
    onEgress.mockClear()
  })

  test('advances to next line', () => {
    const node = {
      ...nodeDefaults,
      id: 'nopNode',
      row: 0,
      column: 0,
      linesOfCode: ['NOP', 'ADD 1'],
      curLineIndex: 0,
      acc: 2,
    }
    const lineOfCode = node.linesOfCode[node.curLineIndex]
    const adjacentNodes = {}

    const expected = scanToNextCommand(node)

    const result = executeNop({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    })
    expect(result).toEqual(expected)
  })
})
