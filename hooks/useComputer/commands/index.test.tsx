import { Command, findCommand } from '.'

describe('findCommand', () => {
  test.each([
    ['ADD 0', Command.ADD],
    ['ADD -40', Command.ADD],
    ['ADD 9999999', Command.ADD],
    ['ADD UP', Command.ADD],
    ['ADD,UP', Command.ADD],
    [' ADD UP #wow', Command.ADD],

    ['JEZ BLAH', Command.JEZ],
    ['JEZ BLAH4_ar', Command.JEZ],
    ['JEZ 9999999', Command.JEZ],
    ['JEZ,9999999', Command.JEZ],
    ['  JEZ 9999999#wow', Command.JEZ],

    ['JGZ BLAH', Command.JGZ],
    ['JGZ BLAH4_ar', Command.JGZ],
    ['JGZ 9999999', Command.JGZ],
    ['JGZ,9999999', Command.JGZ],
    ['  JGZ 9999999#wow', Command.JGZ],

    ['JLZ BLAH', Command.JLZ],
    ['JLZ BLAH4_ar', Command.JLZ],
    ['JLZ 9999999', Command.JLZ],
    ['JLZ,9999999', Command.JLZ],
    ['  JLZ 9999999#wow', Command.JLZ],

    ['JMP BLAH', Command.JMP],
    ['JMP BLAH4_ar', Command.JMP],
    ['JMP 9999999', Command.JMP],
    ['JMP,9999999', Command.JMP],
    ['  JMP 9999999#wow', Command.JMP],

    ['JNZ BLAH', Command.JNZ],
    ['JNZ BLAH4_ar', Command.JNZ],
    ['JNZ 9999999', Command.JNZ],
    ['JNZ,9999999', Command.JNZ],
    ['  JNZ 9999999#wow', Command.JNZ],

    ['JRO -1', Command.JRO],
    ['JRO 0', Command.JRO],
    ['JRO 1', Command.JRO],
    ['JRO,1', Command.JRO],
    [' JRO 1 ####', Command.JRO],

    ['MOV -1, ACC', Command.MOV],
    ['MOV 0, LEFT', Command.MOV],
    ['MOV DOWN ACC', Command.MOV],
    ['MOV,DOWN,ACC', Command.MOV],
    [' MOV RIGHT, RIGHT # wow', Command.MOV],

    ['NEG', Command.NEG],
    ['NEG #0 LEFT', Command.NEG],
    ['NEG,#0 LEFT', Command.NEG],
    ['          NEG', Command.NEG],

    ['NOP', Command.NOP],
    ['NOP #0 LEFT', Command.NOP],
    ['NOP,#0 LEFT', Command.NOP],
    ['          NOP', Command.NOP],

    ['SAV', Command.SAV],
    ['SAV #0 LEFT', Command.SAV],
    ['SAV,#0 LEFT', Command.SAV],
    ['          SAV', Command.SAV],

    ['SUB 0', Command.SUB],
    ['SUB -40', Command.SUB],
    ['SUB 9999999', Command.SUB],
    ['SUB,9999999', Command.SUB],
    ['SUB UP', Command.SUB],
    [' SUB UP #wow', Command.SUB],

    ['SWP', Command.SWP],
    ['SWP #0 LEFT', Command.SWP],
    ['SWP,#0 LEFT', Command.SWP],
    ['          SWP', Command.SWP],
  ])('"%s" matches to %s command', (lineOfCode: string, expected: Command) => {
    expect(findCommand(lineOfCode).type).toEqual(expected)
  })
})
