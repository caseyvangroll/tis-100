import { useState, useMemo, useCallback } from 'react'
import useLocalStorage from '../useLocalStorage'
import { Tile, LAYOUT_WIDTH } from '../../constants'
import { InitialNodeState, ComputerState, ComputerType, TestInputValues, TestOutputValues, TestNodeConfig, ComputeNode, OnEgress, OnIngress } from '../../types'
import {
  flagAdjacentNodes,
  getInputNodeLinesOfCode,
  getOutputNodeLinesOfCode,
  resetNode,
  resolveInputNodeDefaults,
  resolveOutputNodeDefaults,
  resolveNodeDefaults,
  updateNodeLinesOfCode,
} from './nodes'
import { prepareCommand, executeCommand } from './commands'

export const getDefaultState = (
  layout: Array<Tile>,
  inputNodes: Array<TestNodeConfig>,
  outputNodes: Array<TestNodeConfig>,
  initialNodeStates?: Array<InitialNodeState>
): ComputerState => {
  const nodes = [...resolveNodeDefaults(layout, initialNodeStates), ...resolveInputNodeDefaults(inputNodes), ...resolveOutputNodeDefaults(outputNodes)]
  return {
    nodes: flagAdjacentNodes(nodes),
    isRunning: false,
  }
}

export default function useComputer(
  slug: string,
  layout: Array<Tile>,
  inputNodes: Array<TestNodeConfig>,
  outputNodes: Array<TestNodeConfig>,
  initialNodeStates?: Array<InitialNodeState>
): ComputerType {
  const defaultState = useMemo(() => getDefaultState(layout, inputNodes, outputNodes, initialNodeStates), [layout, inputNodes, outputNodes, initialNodeStates])

  const [nodes, setNodes] = useLocalStorage(slug, defaultState.nodes)
  const [isRunning, setIsRunning] = useState(defaultState.isRunning)

  const setNodeLinesOfCode = useCallback(
    (row: number, column: number, linesOfCode: Array<string>, cursorIndex: number) => {
      const nodeToUpdateIndex = row * LAYOUT_WIDTH + column
      setNodes((prevNodes) => prevNodes.map((node, i) => (i === nodeToUpdateIndex ? updateNodeLinesOfCode(node, linesOfCode, cursorIndex) : node)))
    },
    [setNodes]
  )

  const step = useCallback(
    (onEgress: OnEgress, onIngress: OnIngress) => {
      setNodes((prevNodes) => {
        const preparedNodes = prevNodes.map(prepareCommand)
        return preparedNodes.map((node: ComputeNode) => executeCommand(node, preparedNodes, onEgress, onIngress))
      })
      setIsRunning(true)
    },
    [setNodes]
  )

  const start = useCallback(() => {
    setIsRunning(true)
  }, [setIsRunning])

  const stop = useCallback(() => {
    setNodes((prevNodes) => prevNodes.map(resetNode))
    setIsRunning(false)
  }, [setNodes, setIsRunning])

  const setInputValues = useCallback(
    (inputs: Array<TestInputValues>) => {
      setNodes((prevNodes) =>
        prevNodes.map((node) => {
          const matchingInput = inputs.find((input) => input.id === node.id)
          if (matchingInput) {
            return {
              ...node,
              curLineIndex: -1,
              linesOfCode: getInputNodeLinesOfCode(matchingInput.valuesToSend),
            }
          }
          return node
        })
      )
    },
    [setNodes]
  )

  const setOutputValues = useCallback(
    (outputs: Array<TestOutputValues>) => {
      setNodes((prevNodes) =>
        prevNodes.map((node) => {
          const matchingOutput = outputs.find((output) => output.id === node.id)
          if (matchingOutput) {
            return {
              ...node,
              curLineIndex: -1,
              linesOfCode: getOutputNodeLinesOfCode(matchingOutput.expectedValues),
            }
          }
          return node
        })
      )
    },
    [setNodes]
  )

  return {
    nodes,
    isRunning,
    setNodeLinesOfCode,
    setInputValues,
    setOutputValues,
    start,
    step,
    stop,
  }
}
