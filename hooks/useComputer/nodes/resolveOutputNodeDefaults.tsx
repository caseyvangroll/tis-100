import { TestNodeConfig, ComputeNode } from '../../../types'
import { nodeDefaults } from './resolveNodeDefaults'
import { NodeType, LAYOUT_HEIGHT } from '../../../constants'

function resolveOutputNodeDefaults(initialOutputs?: Array<TestNodeConfig>): Array<ComputeNode> {
  return (initialOutputs || []).map(({ id, column }) => ({
    ...nodeDefaults,
    id,
    type: NodeType.Output,
    row: LAYOUT_HEIGHT,
    column,
    linesOfCode: ['MOV UP ACC'],
  }))
}

export default resolveOutputNodeDefaults
