import { NodeType } from '../../../constants'
import { TestNodeConfig, ComputeNode } from '../../../types'
import { nodeDefaults } from './resolveNodeDefaults'

function resolveInputNodeDefaults(initialInputs?: Array<TestNodeConfig>): Array<ComputeNode> {
  return (initialInputs || []).map(({ id, column }) => ({
    ...nodeDefaults,
    id,
    type: NodeType.Input,
    row: -1,
    column,
    linesOfCode: [],
  }))
}

export default resolveInputNodeDefaults
