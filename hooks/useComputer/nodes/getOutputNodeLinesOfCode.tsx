const getOutputNodeLinesOfCode = (expectedValues: Array<number>): Array<string> => [...expectedValues.map(() => `MOV UP ACC`), 'NOP', 'JRO -1']

export default getOutputNodeLinesOfCode
