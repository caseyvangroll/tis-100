import { ComputeNode } from '../../../types'
import { Mode } from '../../../constants'

function resetNode(node: ComputeNode): ComputeNode {
  return {
    id: node.id,
    type: node.type,
    row: node.row,
    column: node.column,
    cursorIndex: 0,
    linesOfCode: node.linesOfCode,
    curLineIndex: -1,
    isDisabled: node.isDisabled,
    hasError: false,
    acc: 0,
    bak: 0,
    mode: Mode.Idle,
    hasUp: node.hasUp,
    hasRight: node.hasRight,
    hasDown: node.hasDown,
    hasLeft: node.hasLeft,
  }
}

export default resetNode
