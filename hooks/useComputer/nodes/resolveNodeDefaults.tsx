import { InitialNodeState, ComputeNode } from '../../../types'
import { Tile, Mode, NodeType, LAYOUT_WIDTH, LAYOUT_HEIGHT } from '../../../constants'

export const nodeDefaults = {
  type: NodeType.Compute,
  cursorIndex: 0,
  linesOfCode: [],
  curLineIndex: -1,
  isDisabled: false,
  hasError: false,
  acc: 0,
  bak: 0,
  hasUp: false,
  hasRight: false,
  hasDown: false,
  hasLeft: false,
  mode: Mode.Idle,
}

function resolveNodeDefaults(layout: Array<Tile>, initialState?: Array<InitialNodeState>): Array<ComputeNode> {
  const nodes: Array<ComputeNode> = []
  for (let row = 0; row < LAYOUT_HEIGHT; row += 1) {
    for (let column = 0; column < LAYOUT_WIDTH; column += 1) {
      const node = {
        ...nodeDefaults,
        id: `node[${row},${column}]`,
        row,
        column,
        ...(initialState?.[row * LAYOUT_WIDTH + column] || {}),
      }

      const nodeType = layout?.[row * LAYOUT_WIDTH + column]
      if (nodeType === Tile.Damaged) {
        node.isDisabled = true
      }

      nodes.push(node)
    }
  }
  return nodes
}

export default resolveNodeDefaults
