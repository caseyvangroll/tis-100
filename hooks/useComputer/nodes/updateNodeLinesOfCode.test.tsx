import { sanitizeLinesOfCode } from './updateNodeLinesOfCode'
import { MAX_LINE_OF_CODE_LENGTH, MAX_LINES_OF_CODE } from '../../../constants'

describe('sanitizeLinesOfCode', () => {
  test('trims all lines to MAX_LINE_OF_CODE_LENGTH', () => {
    const linesOfCode = ['A'.repeat(MAX_LINE_OF_CODE_LENGTH + 1), 'B'.repeat(MAX_LINE_OF_CODE_LENGTH + 1)]
    const cursorIndex = linesOfCode.length - 1

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['A'.repeat(MAX_LINE_OF_CODE_LENGTH), 'B'.repeat(MAX_LINE_OF_CODE_LENGTH)]
    expect(actualLinesOfCode).toEqual(expected)
  })

  test('trims line count to MAX_LINES_OF_CODE', () => {
    const linesOfCode = new Array(MAX_LINES_OF_CODE + 1).fill('A')
    const cursorIndex = linesOfCode.length - 1

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = new Array(MAX_LINES_OF_CODE).fill('A')
    expect(actualLinesOfCode).toEqual(expected)
  })

  test('capitalizes all lines', () => {
    const linesOfCode = ['a'.repeat(MAX_LINE_OF_CODE_LENGTH), 'b'.repeat(MAX_LINE_OF_CODE_LENGTH)]
    const cursorIndex = linesOfCode.length - 1

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['A'.repeat(MAX_LINE_OF_CODE_LENGTH), 'B'.repeat(MAX_LINE_OF_CODE_LENGTH)]
    expect(actualLinesOfCode).toEqual(expected)
  })

  // This test is coupled to MAX_LINE_OF_CODE_LENGTH value
  test('trims characters from behind cursor first if line exceeds length (first and last line)', () => {
    const linesOfCode = ['12345XXXXXXXXXXXXX67890']
    const cursorIndex = linesOfCode[0].indexOf('6')

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['12345XXXXXXXX67890']
    expect(actualLinesOfCode).toEqual(expected)
  })

  // This test is coupled to MAX_LINE_OF_CODE_LENGTH value
  test('trims characters from behind cursor first if line exceeds length by one (first and last line)', () => {
    const linesOfCode = ['12345XXXXXXXXX67890']
    const cursorIndex = linesOfCode[0].indexOf('6')

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['12345XXXXXXXX67890']
    expect(actualLinesOfCode).toEqual(expected)
  })

  // This test is coupled to MAX_LINE_OF_CODE_LENGTH value
  test('trims characters from behind cursor first if line exceeds length (middle line)', () => {
    const linesOfCode = ['I LOVE BACON IT IS GOOD', '12345XXXXXXXXXXXXX67890', 'DO YOU LOVE BACON TOO']
    const cursorIndex = linesOfCode[0].length + linesOfCode[1].indexOf('6')

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['I LOVE BACON IT IS', '12345XXXXXXXX67890', 'DO YOU LOVE BACON ']
    expect(actualLinesOfCode).toEqual(expected)
  })

  // This test is coupled to MAX_LINE_OF_CODE_LENGTH value
  test('trims characters from behind cursor first if line exceeds length by one (middle line)', () => {
    const linesOfCode = ['I LOVE BACON IT IS GOOD', '12345XXXXXXXXX67890', 'DO YOU LOVE BACON TOO']
    const cursorIndex = linesOfCode[0].length + linesOfCode[1].indexOf('6')

    const [actualLinesOfCode] = sanitizeLinesOfCode(linesOfCode, cursorIndex)
    const expected = ['I LOVE BACON IT IS', '12345XXXXXXXX67890', 'DO YOU LOVE BACON ']
    expect(actualLinesOfCode).toEqual(expected)
  })
})
