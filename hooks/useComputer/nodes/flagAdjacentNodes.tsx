import { NodeType } from '../../../constants'
import { ComputeNode } from '../../../types'

function flagAdjacentNodes(initialNodes: Array<ComputeNode>): Array<ComputeNode> {
  return initialNodes.map((node) => {
    return {
      ...node,
      hasUp: initialNodes.some(({ row, column, type }) => row === node.row - 1 && column === node.column && type !== NodeType.Input),
      hasRight: initialNodes.some(({ row, column }) => row === node.row && column === node.column + 1),
      hasDown: initialNodes.some(({ row, column }) => row === node.row + 1 && column === node.column),
      hasLeft: initialNodes.some(({ row, column }) => row === node.row && column === node.column - 1),
    }
  })
}

export default flagAdjacentNodes
