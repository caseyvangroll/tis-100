import { useState, useEffect, useMemo, SetStateAction, Dispatch } from 'react'

function useLocalStorage<T>(key: string, defaultValue: T): [T, Dispatch<SetStateAction<T>>] {
  const previousValue = useMemo(() => {
    if (typeof window !== 'undefined') {
      return window.localStorage.getItem(key)
    }
    return null
  }, [key])

  const [value, setValue] = useState(previousValue ? JSON.parse(previousValue) : defaultValue)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.localStorage.setItem(key, JSON.stringify(value))
    }
  }, [key, value])

  return [value, setValue]
}

export default useLocalStorage
