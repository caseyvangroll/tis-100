import { Tile } from '../../constants'
import { RandomIntGenerator, TestValues } from '../../types'

const IN_A = 'IN.A'
const OUT_A = 'OUT.A'

const puzzle = {
  slug: 'signal-amplifier',
  name: 'SIGNAL AMPLIFIER',
  description: [`READ A VALUE FROM ${IN_A}`, 'DOUBLE THE VALUE', `WRITE THE VALUE TO ${OUT_A}`],
  creator: 'ZACHTRONICS',
  layout: [Tile.Compute, Tile.Compute, Tile.Compute, Tile.Damaged, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Damaged, Tile.Compute, Tile.Compute, Tile.Compute],
  inputNodes: [
    {
      id: IN_A,
      column: 1,
    },
  ],
  outputNodes: [
    {
      id: OUT_A,
      column: 2,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const aInputValues = new Array(39).fill(0).map(() => getRandomInt(10, 99))
    const aOutputValues = aInputValues.map((n) => n * 2)
    return {
      inputs: [
        {
          id: IN_A,
          valuesToSend: aInputValues,
        },
      ],
      outputs: [
        {
          id: OUT_A,
          expectedValues: aOutputValues,
        },
      ],
    }
  },
}

export default puzzle
