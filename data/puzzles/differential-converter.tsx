import { Tile } from '../../constants'
import { RandomIntGenerator, TestValues } from '../../types'

const IN_A = 'IN.A'
const IN_B = 'IN.B'
const OUT_P = 'OUT.P'
const OUT_N = 'OUT.N'

const puzzle = {
  slug: 'differential-converter',
  name: 'DIFFERENTIAL CONVERTER',
  description: [`READ VALUES FROM ${IN_A} AND ${IN_B}`, `WRITE ${IN_A} - ${IN_B} TO ${OUT_P}`, `WRITE ${IN_B} - ${IN_A} TO ${OUT_N}`],
  creator: 'ZACHTRONICS',
  layout: [Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Damaged, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute],
  inputNodes: [
    {
      id: IN_A,
      column: 1,
    },
    {
      id: IN_B,
      column: 2,
    },
  ],
  outputNodes: [
    {
      id: OUT_P,
      column: 1,
    },
    {
      id: OUT_N,
      column: 2,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const aInputValues = new Array(39).fill(0).map(() => getRandomInt(10, 99))
    const bInputValues = new Array(39).fill(0).map(() => getRandomInt(10, 99))
    const pOutputValues = aInputValues.map((a, i) => a - bInputValues[i])
    const nOutputValues = bInputValues.map((b, i) => b - aInputValues[i])
    return {
      inputs: [
        {
          id: IN_A,
          valuesToSend: aInputValues,
        },
        {
          id: IN_B,
          valuesToSend: bInputValues,
        },
      ],
      outputs: [
        {
          id: OUT_P,
          expectedValues: pOutputValues,
        },
        {
          id: OUT_N,
          expectedValues: nOutputValues,
        },
      ],
    }
  },
}

export default puzzle
