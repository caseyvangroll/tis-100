import { Tile } from '../../constants'
import { RandomIntGenerator, TestValues } from '../../types'

const IN = 'IN'
const OUT_G = 'OUT.G'
const OUT_E = 'OUT.E'
const OUT_L = 'OUT.L'

const puzzle = {
  slug: 'signal-comparator',
  name: 'SIGNAL COMPARATOR',
  description: [
    `READ A VALUE FROM ${IN}`,
    `WRITE 1 TO ${OUT_G} IF ${IN} > 0`,
    `WRITE 1 TO ${OUT_E} IF ${IN} = 0`,
    `WRITE 1 TO ${OUT_L} IF ${IN} < 0`,
    'WHEN A 1 IS NOT WRITTEN TO AN OUTPUT, WRITE A 0 INSTEAD',
  ],
  creator: 'ZACHTRONICS',
  layout: [Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Damaged, Tile.Damaged, Tile.Damaged, Tile.Compute, Tile.Compute, Tile.Compute, Tile.Compute],
  inputNodes: [
    {
      id: IN,
      column: 0,
    },
  ],
  outputNodes: [
    {
      id: OUT_G,
      column: 1,
    },
    {
      id: OUT_E,
      column: 2,
    },
    {
      id: OUT_L,
      column: 3,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const inputValues = new Array(39).fill(0).map(() => getRandomInt(-2, 2))
    const gOutputValues = inputValues.map((s) => (s > 0 ? 1 : 0))
    const eOutputValues = inputValues.map((s) => (s === 0 ? 1 : 0))
    const lOutputValues = inputValues.map((s) => (s < 0 ? 1 : 0))
    return {
      inputs: [
        {
          id: IN,
          valuesToSend: inputValues,
        },
      ],
      outputs: [
        {
          id: OUT_G,
          expectedValues: gOutputValues,
        },
        {
          id: OUT_E,
          expectedValues: eOutputValues,
        },
        {
          id: OUT_L,
          expectedValues: lOutputValues,
        },
      ],
    }
  },
}

export default puzzle
