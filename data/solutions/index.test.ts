import fs from 'fs'
import { getInitialNodeStatesForSolution, parseNode } from './index'

describe('parseNode', () => {
  test.each([
    [
      '@0\n\n\n',
      {
        row: 0,
        column: 0,
        linesOfCode: [],
      },
    ],
    [
      '@1\nMOV UP DOWN\n\n',
      {
        row: 0,
        column: 1,
        linesOfCode: ['MOV UP DOWN'],
      },
    ],
    [
      '@4\nMOV UP DOWN\nMOV UP RIGHT\n\n',
      {
        row: 1,
        column: 0,
        linesOfCode: ['MOV UP DOWN', 'MOV UP RIGHT'],
      },
    ],
    [
      '@5\nMOV LEFT ACC\nADD ACC\nMOV ACC DOWN\n\n',
      {
        row: 1,
        column: 1,
        linesOfCode: ['MOV LEFT ACC', 'ADD ACC', 'MOV ACC DOWN'],
      },
    ],
  ])('should return expected for "%s"', (rawNodeText, expected) => {
    expect(parseNode(rawNodeText)).toEqual(expected)
  })
})

describe('getInitialNodeStatesForSolution', () => {
  test('should work for signal-amplifier.txt', () => {
    const signalAmplifierRawSolution = fs.readFileSync('data/solutions/signal-amplifier.txt', 'utf-8')
    const actual = getInitialNodeStatesForSolution(signalAmplifierRawSolution)
    expect(actual).toEqual([
      {
        column: 0,
        linesOfCode: [],
        row: 0,
      },
      {
        column: 1,
        linesOfCode: ['MOV UP DOWN'],
        row: 0,
      },
      {
        column: 2,
        linesOfCode: [],
        row: 0,
      },
      {
        column: 3,
        linesOfCode: [],
        row: 0,
      },
      {
        column: 0,
        linesOfCode: ['MOV UP DOWN', 'MOV UP RIGHT'],
        row: 1,
      },
      {
        column: 1,
        linesOfCode: ['MOV LEFT ACC', 'ADD ACC', 'MOV ACC DOWN'],
        row: 1,
      },
      {
        column: 2,
        linesOfCode: [],
        row: 1,
      },
      {
        column: 3,
        linesOfCode: ['MOV UP ACC', 'ADD ACC', 'MOV ACC RIGHT'],
        row: 1,
      },
      {
        column: 0,
        linesOfCode: ['MOV ANY DOWN'],
        row: 2,
      },
      {
        column: 1,
        linesOfCode: [],
        row: 2,
      },
      {
        column: 2,
        row: 2,
      },
      {
        column: 3,
        row: 2,
      },
    ])
  })
})
