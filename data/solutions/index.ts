import { InitialNodeState } from '../../types'
import { LAYOUT_WIDTH } from '../../constants'

function getDefaultNode(i: number) {
  return {
    row: Math.floor(i / LAYOUT_WIDTH),
    column: i % LAYOUT_WIDTH,
  }
}

export function parseNode(chunk: string): InitialNodeState {
  const linesOfCode = chunk
    .split(/\n/g)
    .map((s) => s.trim())
    .filter(Boolean)
  const indexLine = linesOfCode.shift() || ''
  const [, i] = indexLine.match(/@(\d+)/) || []
  return {
    ...getDefaultNode(parseInt(i, 10)),
    linesOfCode,
  }
}

export function getInitialNodeStatesForSolution(solution: string): InitialNodeState[] {
  const chunks = solution.match(/@\d[^@]*/g) || []
  const nodes: InitialNodeState[] = []
  chunks.forEach((chunk) => {
    nodes.push(parseNode(chunk))
  })
  for (let i = nodes.length; i < 12; i += 1) {
    nodes.push(getDefaultNode(i))
  }
  return nodes
}
