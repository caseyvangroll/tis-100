/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
  eslint: {
    dirs: [
      '__test__',
      'components',
      'data',
      'hooks',
      'pages',
      'utils',
    ],
  },
};

module.exports = nextConfig;
