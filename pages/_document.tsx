import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html className="h-full p-0 m-0">
        <Head>
          {/* Meta */}
          <meta name="description" content="Web app simulating the TIS-100" />

          {/* Favicon */}
          <link rel="icon" href="/favicon.ico" sizes="any" />
          <link rel="manifest" href="/manifest.webmanifest" />

          {/* Font */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
          <link href="https://fonts.googleapis.com/css2?family=Ubuntu+Mono&display=swap" rel="stylesheet" />
        </Head>
        <body className="min-h-full flex items-center p-0 lg:mx-2 xl:mx-10 bg-black text-white font-mono uppercase">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
