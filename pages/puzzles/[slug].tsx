import type { NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Computer, TestDisplay, PuzzleStatus, Controls, Nav } from '../../components'
import usePuzzle from '../../hooks/usePuzzle'
import { PuzzleConfiguration, InitialNodeState } from '../../types'
import puzzles from '../../data/puzzles'

export interface PuzzlePureProps extends PuzzleConfiguration {
  initialNodeStates?: Array<InitialNodeState>
}

export const PuzzlePure: React.FC<PuzzlePureProps> = ({ name, slug, description, creator, layout, inputNodes, outputNodes, getTestValues, initialNodeStates }) => {
  const { computer, testIndex, inputs, outputs, start, startFast, step, stop, stepSpeed } = usePuzzle(slug, layout, inputNodes, outputNodes, getTestValues, initialNodeStates)

  return (
    <>
      <Head>
        <title>TIS-100 | {name}</title>
        <meta name="description" content="Web app simulating the TIS-100" />
      </Head>
      <div id="puzzle" className="flex flex-row flex-nowrap justify-start 2xl:justify-center">
        <aside className="flex flex-col items-center justify-between xl:mr-4 w-72 xl:w-96">
          <Nav />
          <div className="flex w-full">
            <span className="flex flex-col grow justify-center">
              <h4 className="pointer-events-none text-center">{name}</h4>
              <h5 className="pointer-events-none mb-2 text-center">by {creator}</h5>
            </span>
          </div>
          <PuzzleStatus description={description} testIndex={testIndex} isRunning={computer.isRunning} />
          <TestDisplay inputs={inputs} outputs={outputs} isRunning={computer.isRunning} />
          <Controls start={start} startFast={startFast} step={step} stop={stop} isRunning={computer.isRunning} testIndex={testIndex} stepSpeed={stepSpeed} />
        </aside>
        <main className="flex items-center">
          <Computer {...computer} />
        </main>
      </div>
    </>
  )
}

const Puzzle: NextPage = () => {
  const router = useRouter()
  const { slug } = router.query
  const puzzleConfiguration = puzzles.find((puzzle: PuzzleConfiguration) => slug === puzzle.slug)

  if (puzzleConfiguration) {
    return <PuzzlePure {...puzzleConfiguration} />
  }
  return <div>404 O&apos;Clock</div>
}

export async function getStaticPaths() {
  return {
    paths: puzzles.map((puzzle) => ({
      params: { slug: puzzle.slug },
    })),
    fallback: false,
  }
}

export async function getStaticProps() {
  return { props: {} }
}

export default Puzzle
