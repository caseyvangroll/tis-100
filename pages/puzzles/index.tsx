import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import puzzles from '../../data/puzzles'

const PuzzlesPage: NextPage = () => {
  return (
    <>
      <Head>
        <title>TIS-100 | PUZZLES</title>
        <meta name="description" content="Web app simulating the TIS-100" />
      </Head>

      <main className="flex justify-center">
        <div className="flex flex-col items-center mt-8">
          <div className="flex w-full static md:relative items-center justify-center">
            <span className="absolute left-8">
              <Link href="/" passHref>
                <button className="box-shadow m-0 py-0 px-4 w-full">
                  <a>BACK</a>
                </button>
              </Link>
            </span>
            <h1>Puzzles</h1>
          </div>
          <nav className="grid sm:grid-cols-2 grid-cols-1 mt-2">
            {puzzles.map(({ slug, name }) => (
              <Link href={`/puzzles/${slug}`} key={slug} passHref>
                <button className="box-shadow">
                  <a className="text-center no-underline p-4">{name}</a>
                </button>
              </Link>
            ))}
          </nav>
        </div>
      </main>
    </>
  )
}

export default PuzzlesPage
