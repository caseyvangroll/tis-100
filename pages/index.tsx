import type { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'

const LandingPage: NextPage = () => {
  return (
    <>
      <Head>
        <title>TIS-100</title>
        <meta name="description" content="Web app simulating the TIS-100" />
      </Head>

      <main className="flex flex-col w-full h-full items-center justify-center">
        <h1 className="pointer-events-none bg-black">TIS-100</h1>
        <Link href="/puzzles" passHref>
          <button className="box-shadow bg-black">
            <h2>
              <a>SIMULATE</a>
            </h2>
          </button>
        </Link>
        <button className="box-shadow bg-black">
          <a href="https://caseyvangroll.com" target="_blank" rel="noreferrer">
            MADE BY CASEY
          </a>
        </button>
      </main>
    </>
  )
}

export default LandingPage
